/*
  Comandos.cpp - Libreria de Cadenas para comunicacion con Smart One.
  Creada por Jefferson Beleño, Diciembre 3, 2020.
*/

#include <Arduino.h>
#include "Comandos.h"
#include <string.h>

Comandos::Comandos(int garbage){}

//------------------------------------------------
//Generacion de cadenas para enviar al Smart One
//------------------------------------------------
String Comandos::PromedioNivelGas(float t1, float t2, float t3, float t) {
  _texto="1,3,"+String(t1,1)+","+String(t2,1)+","+String(t3,1)+","+String(t,1)+",L";
  _check = Comandos::ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}

String Comandos::PromedioNivelGas(float t1, float t2, float t3, float t4, float t) {
  _texto="1,4,"+String(t1,1)+","+String(t2,1)+","+String(t3,1)+","+String(t4,1)+","+String(t,1)+",L";
  _check = Comandos::ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}

String Comandos::PromedioFlujoGas(float t1, float t2, float t3, float t) {
  _texto="2,3,"+String(t1,1)+","+String(t2,1)+","+String(t3,1)+","+String(t,1)+",L/h";
  _check = Comandos::ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}

String Comandos::ConsumoAcumuladoGas(float t1) {
  _texto="3,"+String(t1,1)+",L";
  _check = Comandos::ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}

String Comandos::DiferenciaGas(float t1){
  _texto="4,"+String(t1,1)+",L";
  _check = Comandos::ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}

//String AlertaEvento(String E,String lat,String lon,String fecha,String hora){
//  String _texto = "A," +E+ "," +lat+ "," +lon+ "," +fecha+ "," +hora;
//  String _check = ObtenerChecksum(_texto);
//  char longitud = _texto.length()+10;
//  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
//}

String Comandos::PosDate(String lat,String lon,String fecha,String hora){
  _texto="F,"+lat+","+lon+","+fecha+","+hora;
  _check = ObtenerChecksum(_texto);
  longitud = _texto.length()+10;
  return String(longitud)+"'"+"'"+"$"+_texto+"*"+_check;
}
//--------------------------------------------------
//Generacion de Checksum para los comandos
//--------------------------------------------------
uint8_t Comandos::XORChecksum8(const byte *data, size_t dataLength) {
  uint8_t value = 0;
  for (size_t i = 0; i < dataLength; i++) {
    value ^= (uint8_t)data[i];
  }
  return value;
}

String Comandos::ObtenerChecksum(String dat) {
  dat.getBytes(_msg2, 103);
  _checksum = XORChecksum8(_msg2, sizeof(_msg2) / sizeof(_msg2[0]));
  return String(_checksum, HEX);
}
