//
//Inclusión de librerias
#include <NMEA2000.h>
#include <N2kMessages.h>
#include <NMEA2000_Teensyx.h>
#include <SD.h>
#include <TimeLib.h>
//Comandos para SmartOne
#include "Comandos.h"

///Variables Modulo Gps
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////
//Smart One
Comandos comand(1);
String texto;
byte mensaje[55];
uint16_t crcval;
float tanque[4] = {152.1,245.8,278.0,272.8};
float total = 2762.9;
const int handshake_Pin = 18;

void EnviarSmartOne(){
  texto.getBytes(mensaje,56);
  uint16_t lon = mensaje[0]-2;
  uint8_t i=54;
  while(i!=0){
    i--;
    mensaje[i+1]=mensaje[i];
  }
  mensaje[0]=0xAA;
  mensaje[3]=0x10;
  uint8_t sum1 = mensaje[lon-2];
  uint8_t sum2 = mensaje[lon-1];
  if(sum1>60) mensaje[lon-2]=sum1-0x20;
  if(sum2>60) mensaje[lon-1]=sum2-0x20;
  crcval = crc16(mensaje,lon);
  digitalWrite(handshake_Pin , LOW);
  delay(3);
  //Serial.write(mensaje,lon);
  for(int i=0;i<lon;i++){
    Serial4.print(mensaje[i],HEX);
  }
  Serial4.println(crcval,HEX);
  //recibir respuesta
  digitalWrite(handshake_Pin , HIGH);
}
/////////////////////////////////////////////////////////////////////////////////
  // *** Use a statically-defined array of bytes, like this:
  char valor[11] = {0xAA, 0x0D, 0x27, 0x10, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
  // char valor[3] = {0xAA,0x05,0x26};
  //char valor[7] = {0xAA,0x09,0x01,0x00,0x28,0x4E,0xD6};
  char bytescrcVal[4];

// *** Dropping all the word references and using std int types instead
uint16_t  crc16 (char *pData, uint16_t length)
{
  // *** Using uint8_t instead of byte.  They're equivalent, so it's a matter
  // of preference, but I believe in using a type that defines the variable's
  // purpose -- a byte would signify to me that its contents is probably
  // binary, whereas uint8_t just tells me 'i' is an 8-bit unsigned integer.
  uint8_t  i,lsb;
  uint16_t  data, crc;    // *** Again, dropping the word type
  crc = 0xFFFF;

  // *** I'm removing the "if length == 0" thing and using a loop where
  // the while conditional is up top.  This skips the loop when length == 0,
  // so there's no need to handle that case specifically.  The only difference
  // in functionality is what gets returned when length == 0.  Before, the
  // code returned "0x0000".  Now, it returns "0xFFFF" -- the crc seed
  // value.  I think this is actually what's supposed to happen, but if I'm
  // wrong, undo that change.
  while (length--) {

    // *** You need to cast "*pData" to an 8-bit type here to operate on
    // one byte at a time, otherwise the compiler will use the default int
    // type -- which is 16-bit, since the pointers refers to a 16-bit type.
    // You don't, however, need to cast the literal (0x00FF) to a 16-bit
    // type.  It already is.  (32-bit/64-bit computers would be another
    // matter though...)
    data = 0x00FF & (uint8_t) * pData++;
    crc = crc ^ data;

    for (i = 8; i > 0; i--)
    {
      if (crc & 0x0001)
        crc = (crc >> 1) ^ 0x8408;
      else
        crc >>= 1;
    }
  }

  crc = ~crc;
  i = crc >> 8;
  lsb = crc - (lsb << 8);
  crc = (lsb<<8) + i;
  Serial.println(crc, HEX);
  Serial.println(crc);

  return (crc);
}

//////////////////////////////////////////////////////////////////////////////////
//GPS
  uint8_t s,k,dia,mes,yea,hora,minuto,segundo;
  uint8_t datosgps;
  String DatosGps[12];
  String aux,Latitud,Longitud;
  const int offset = -5;  // Bogota GMT-5
  
bool GpsParser(char c){
  boolean parsed = false;
  switch(c){
    case '$':
      if(s==0) s=1;
      else s=0;
      break;
    case 'G':
      if(s==1) s=2;
      else s=0;
      break;
    case 'P':
      if(s==2) s=3;
      else s=0;
      break;
    case 'R':
      if(s==3) s=4;
      else s=0;
      break;
    case 'M':
      if(s==4) s=5;
      else s=0;
      break;
    case 'C':
      if(s==5){
        s=6;
        datosgps=0;
      }
      else s=0;
      break;
    case ',':
      if(s==6){
        s=7; k=0;
        for(int i=0;i<12;i++){
          DatosGps[i]="";
        }
      }
      else if(s>6){
        k++;
      }
      else s=0;
      break;
    case '*':
      if(s==7){
        if(datosgps > 42 ) parsed = true;
        s=0;
      }
      break;
    default:
      if(s==7){
        datosgps++;
        DatosGps[k].concat(c);
      }
      else s=0;
      break;
  }
  return parsed;
}
void GpsPosicion(){
  Latitud = DatosGps[2] + "," + DatosGps[3];
  Longitud = DatosGps[4] + "," + DatosGps[5];
}

void GpsFecha(){
  aux = DatosGps[0].substring(0,2);
  hora = aux.toInt();
  aux = DatosGps[0].substring(2,4);
  minuto = aux.toInt();
  aux = DatosGps[0].substring(4,6);
  segundo = aux.toInt();
  aux = DatosGps[8].substring(0,2);
  dia = aux.toInt();
  aux = DatosGps[8].substring(2,4);
  mes = aux.toInt();
  aux = DatosGps[8].substring(4,6);
  yea = aux.toInt();
}
//time_t ActualizarHora(){
//  Serial.println("actualizando hora...");
  //while(!gps.encode(SerialGPS.read()));
//  GpsFecha();
//  int yearr = yea+2000;
//  setTime(hora, minuto, segundo, dia, mes, yearr);
//  adjustTime(offset * SECS_PER_HOUR);
//  Serial.println("hora actualizada");
//  GpsPosicion();
//  return now();
//}
//////////////////////////////////////////////////////////////////////////////////

//Variables para trabajo con los archivos csv
char *nombreFile;
String message = "";
byte datoLecturaSD; //Variable que toma el dato actual
File myFile;//Objeto tipo File que permite acceder y operar la SD

//Objeto NMEA para control y trabajo
//trabajo con la libreria
tNMEA2000_Teensyx NMEA2000;

//Definicion de Estructura para trabajo
//con la Nmea 2000
typedef struct {
  unsigned long PGN;
  void (*Handler)(const tN2kMsg &N2kMsg);
} tNMEA2000Handler;

/*//Como se leen dos parametros: flujo de combustible
  //Y Nivel de Tanque, se definen las funciones acordes
  con la libreria N2KMessages*/
void FluidLevel(const tN2kMsg &N2kMsg);
void EngineDynamicParam(const tN2kMsg &N2kMsg);

//En este arreglo debe Incluirse el codigo PGN
//Y la direccion de memoria de la función asociada
tNMEA2000Handler NMEA2000Handlers[] = {
  {127505L, &FluidLevel},
  {127489L, &EngineDynamicParam},
  {0, 0}
};

//Variables Creadas Para Operacion
//
bool   banderagalones=true;
bool   banderalitros=false;
bool   banderanudos=true;
bool   banderakm=false;


double tiempopromedio = 60000;

//Variables Procesado de datos Motor 1
double deltaacumulativom1 = 0;
double deltatm1 = 0;
double tiempoAnteriorm1 = 0;
double tiempoActualm1 = 0;
double medidaflujom1 = 0;
double consumoinstm1 = 0;
double Acumuladorconsumom1 = 0;
double consumotiempopromediom1 = 0;
double deltaacumulativohorasm1 = 0;
double flujomediom1 = 0;
double tiempoenhorasm1 = 0;
bool bncompletadom1 = false;

//Variables Procesado de datos Motor 2
double deltaacumulativom2 = 0;
double deltatm2 = 0;
double tiempoAnteriorm2 = 0;
double tiempoActualm2 = 0;
double medidaflujom2 = 0;
double consumoinstm2 = 0;
double Acumuladorconsumom2 = 0;
double consumotiempopromediom2 = 0;
double deltaacumulativohorasm2 = 0;
double flujomediom2 = 0;
double tiempoenhorasm2 = 0;
bool bncompletadom2 = false;

//Variables Procesado de datos Motor 3
double deltaacumulativom3 = 0;
double deltatm3 = 0;
double tiempoAnteriorm3 = 0;
double tiempoActualm3 = 0;
double medidaflujom3 = 0;
double consumoinstm3 = 0;
double Acumuladorconsumom3 = 0;
double consumotiempopromediom3 = 0;
double deltaacumulativohorasm3 = 0;
double flujomediom3 = 0;
double tiempoenhorasm3 = 0;
bool bncompletadom3 = false;

//Variables Tablas caracterizado de
//tanques
double kema = 0.08;

//Para Tanque 1
String datosPorcentajem1[10];
String datosGalonesm1[10];

float numerosPorcentajem1[10];
float numerosGalonesm1[10];
double receptorlvm1 = 0;
double nivelgalonesm1 = 0;
double emam1 = 0;
double prb1 = 0;
bool flagema1 = false;

//Para Tanque 2
String datosPorcentajem2[10];
String datosGalonesm2[10];

float numerosPorcentajem2[10];
float numerosGalonesm2[10];

double receptorlvm2 = 0;
double nivelgalonesm2 = 0;
double emam2 = 0;
double prb2 = 0;
bool flagema2 = false;

//Para tanque 3
String datosPorcentajem3[10];
String datosGalonesm3[10];

float numerosPorcentajem3[10];
float numerosGalonesm3[10];

double receptorlvm3 = 0;
double nivelgalonesm3 = 0;
double emam3 = 0;
double prb3 = 0;
bool flagema3 = false;

//Alarmas
//char instanciaanterior = "5";
int contador = 0;
byte identificador[3] = {0x30, 0x30, 0x30};


//Definicion de funciones desarrolladas
void revisionSerial(void);
void EnvioDeArchivos(void);
void EscrituraSd(void);
void tablas(void);
void tratamientonivel(void);
void tratamientonivel2(void);
void tratamientonivel3(void);
void clasificacionnivel(void);
void clasificacionnivelm2(void);
void clasificacionnivelm3(void);


//Funcion de Configuración
void setup() {
  //Inicializacion de Comunicación serial
  //Tanto para computadora como modulo Bluetooht
  Serial5.begin(9600);//Bluetooth
  Serial.begin(115200);//Consola
  Serial3.begin(9600);//GPS
  Serial4.begin(9600);//SmartOne
  pinMode(handshake_Pin, OUTPUT);
  digitalWrite(handshake_Pin , HIGH);
  // Encabezados Necesarios para funcionamiento
  // de la libreria NMEA2000
  NMEA2000.EnableForward(false);
  NMEA2000.SetMsgHandler(HandleNMEA2000Msg);
  NMEA2000.Open();

  // Inicialización de la tarjeta SD
  if (!SD.begin(BUILTIN_SDCARD))
  {
    Serial.println("No se tiene acceso a la SD card");
    //return;
  }
  Serial.println(" Inicialización realizada.");
  //GPS actualizar hora cada 6 horas
  //setSyncProvider(ActualizarHora);
  //setSyncInterval(6*SECS_PER_HOUR);
  tablas();
}
uint16_t conteomax = 0;
//Ciclo de Ejecución Principal
void loop() {
  //Metodo necesario para el funcionamiento adecuado
  //de la libreria NMEA 2000
  NMEA2000.ParseMessages();
  //bluetooth
  if (Serial5.available()) {
    revisionSerial();
  }
  //motores
  if ((bncompletadom1 == true) && (bncompletadom2 == true)) {
    EscrituraSd();
  }
  //Recibe datos de GPS
  if (Serial3.available()){
    if(GpsParser(Serial3.read())){
      GpsFecha();
      setTime(hora, minuto, segundo, dia, mes, yea);
      adjustTime(offset * SECS_PER_HOUR);
      myFile = SD.open("Log.txt",FILE_WRITE);
      if(myFile){
        char cadena[30];
        sprintf(cadena,"%02d/%02d/%02d %02d:%02d:%02d",dia,mes,yea,hora,minuto,segundo);
        myFile.println(cadena);
        myFile.close();
        Serial.println(cadena);
      }else {
        Serial.println("Error escribiendo en Log.txt");
      }
    }
  }
}

//Codificación Funciones
//TratamientoNivel
void clasificacionnivel () {

  float prom1 = (0 + numerosPorcentajem1[0]) / 2 ;
  float prom2 = (numerosPorcentajem1[0] + numerosPorcentajem1[1]) / 2 ;
  float prom3 = (numerosPorcentajem1[1] + numerosPorcentajem1[2]) / 2 ;
  float prom4 = (numerosPorcentajem1[2] + numerosPorcentajem1[3]) / 2 ;
  float prom5 = (numerosPorcentajem1[3] + numerosPorcentajem1[4]) / 2 ;
  float prom6 = (numerosPorcentajem1[4] + numerosPorcentajem1[5]) / 2 ;
  float prom7 = (numerosPorcentajem1[5] + numerosPorcentajem1[6]) / 2 ;
  float prom8 = (numerosPorcentajem1[6] + numerosPorcentajem1[7]) / 2 ;
  float prom9 = (numerosPorcentajem1[7] + numerosPorcentajem1[8]) / 2 ;
  float prom10 = (numerosPorcentajem1[8] + numerosPorcentajem1[9]) / 2 ;
  if ((receptorlvm1 <= prom1)) {
    nivelgalonesm1 = 0;
  }
  if ((receptorlvm1 > prom1) && (receptorlvm1 <= prom2)) {
    nivelgalonesm1 = numerosGalonesm1[0];
  }
  if ((receptorlvm1 > prom2) && (receptorlvm1 <= prom3)) {
    nivelgalonesm1 = numerosGalonesm1[1];
  }
  if ((receptorlvm1 > prom3) && (receptorlvm1 <= prom4)) {
    nivelgalonesm1 = numerosGalonesm1[2];
  }
  if ((receptorlvm1 > prom4) && (receptorlvm1 <= prom5)) {
    nivelgalonesm1 = numerosGalonesm1[3];
  }
  if ((receptorlvm1 > prom5) && (receptorlvm1 <= prom6)) {
    nivelgalonesm1 = numerosGalonesm1[4];
  }
  if ((receptorlvm1 > prom6) && (receptorlvm1 <= prom7)) {
    nivelgalonesm1 = numerosGalonesm1[5];
  }
  if ((receptorlvm1 > prom7) && (receptorlvm1 <= prom8)) {
    nivelgalonesm1 = numerosGalonesm1[6];
  }
  if ((receptorlvm1 > prom8) && (receptorlvm1 <= prom9)) {
    nivelgalonesm1 = numerosGalonesm1[7];
  }
  if ((receptorlvm1 > prom9) && (receptorlvm1 <= prom10)) {
    nivelgalonesm1 = numerosGalonesm1[8];
  }
  if ((receptorlvm1 > prom10)) {
    nivelgalonesm1 = numerosGalonesm1[9];
  }
  //numerosPorcentajem1[10];
  //numerosGalonesm1[10];
}

//TratamientoNivel
void clasificacionnivelm2 () {
  float prom1 = (0 + numerosPorcentajem2[0]) / 2 ;
  float prom2 = (numerosPorcentajem2[0] + numerosPorcentajem2[1]) / 2 ;
  float prom3 = (numerosPorcentajem2[1] + numerosPorcentajem2[2]) / 2 ;
  float prom4 = (numerosPorcentajem2[2] + numerosPorcentajem2[3]) / 2 ;
  float prom5 = (numerosPorcentajem2[3] + numerosPorcentajem2[4]) / 2 ;
  float prom6 = (numerosPorcentajem2[4] + numerosPorcentajem2[5]) / 2 ;
  float prom7 = (numerosPorcentajem2[5] + numerosPorcentajem2[6]) / 2 ;
  float prom8 = (numerosPorcentajem2[6] + numerosPorcentajem2[7]) / 2 ;
  float prom9 = (numerosPorcentajem2[7] + numerosPorcentajem2[8]) / 2 ;
  float prom10 = (numerosPorcentajem2[8] + numerosPorcentajem2[9]) / 2 ;
  if ((receptorlvm2 <= prom1)) {
    nivelgalonesm2 = 0;
  }
  if ((receptorlvm2 > prom1) && (receptorlvm2 <= prom2)) {
    nivelgalonesm2 = numerosGalonesm2[0];
  }
  if ((receptorlvm2 > prom2) && (receptorlvm2 <= prom3)) {
    nivelgalonesm2 = numerosGalonesm2[1];
  }
  if ((receptorlvm2 > prom3) && (receptorlvm2 <= prom4)) {
    nivelgalonesm2 = numerosGalonesm2[2];
  }
  if ((receptorlvm2 > prom4) && (receptorlvm2 <= prom5)) {
    nivelgalonesm2 = numerosGalonesm2[3];
  }
  if ((receptorlvm2 > prom5) && (receptorlvm2 <= prom6)) {
    nivelgalonesm2 = numerosGalonesm2[4];
  }
  if ((receptorlvm2 > prom6) && (receptorlvm2 <= prom7)) {
    nivelgalonesm2 = numerosGalonesm2[5];
  }
  if ((receptorlvm2 > prom7) && (receptorlvm2 <= prom8)) {
    nivelgalonesm2 = numerosGalonesm2[6];
  }
  if ((receptorlvm2 > prom8) && (receptorlvm2 <= prom9)) {
    nivelgalonesm2 = numerosGalonesm2[7];
  }
  if ((receptorlvm2 > prom9) && (receptorlvm2 <= prom10)) {
    nivelgalonesm2 = numerosGalonesm2[8];
  }
  if ((receptorlvm2 > prom10)) {
    nivelgalonesm2 = numerosGalonesm2[9];
  }
  //numerosPorcentajem1[10];
  //numerosGalonesm1[10];
}

void clasificacionnivelm3 () {
  float prom1 = (0 + numerosPorcentajem3[0]) / 2 ;
  float prom2 = (numerosPorcentajem3[0] + numerosPorcentajem3[1]) / 2 ;
  float prom3 = (numerosPorcentajem3[1] + numerosPorcentajem3[2]) / 2 ;
  float prom4 = (numerosPorcentajem3[2] + numerosPorcentajem3[3]) / 2 ;
  float prom5 = (numerosPorcentajem3[3] + numerosPorcentajem3[4]) / 2 ;
  float prom6 = (numerosPorcentajem3[4] + numerosPorcentajem3[5]) / 2 ;
  float prom7 = (numerosPorcentajem3[5] + numerosPorcentajem3[6]) / 2 ;
  float prom8 = (numerosPorcentajem3[6] + numerosPorcentajem3[7]) / 2 ;
  float prom9 = (numerosPorcentajem3[7] + numerosPorcentajem3[8]) / 2 ;
  float prom10 = (numerosPorcentajem3[8] + numerosPorcentajem3[9]) / 2 ;
  if ((receptorlvm3 <= prom1)) {
    nivelgalonesm3 = 0;
  }
  if ((receptorlvm3 > prom1) && (receptorlvm3 <= prom2)) {
    nivelgalonesm3 = numerosGalonesm3[0];
  }
  if ((receptorlvm3 > prom2) && (receptorlvm3 <= prom3)) {
    nivelgalonesm3 = numerosGalonesm3[1];
  }
  if ((receptorlvm3 > prom3) && (receptorlvm3 <= prom4)) {
    nivelgalonesm3 = numerosGalonesm3[2];
  }
  if ((receptorlvm3 > prom4) && (receptorlvm3 <= prom5)) {
    nivelgalonesm3 = numerosGalonesm3[3];
  }
  if ((receptorlvm3 > prom5) && (receptorlvm3 <= prom6)) {
    nivelgalonesm3 = numerosGalonesm3[4];
  }
  if ((receptorlvm3 > prom6) && (receptorlvm3 <= prom7)) {
    nivelgalonesm3 = numerosGalonesm3[5];
  }
  if ((receptorlvm3 > prom7) && (receptorlvm3 <= prom8)) {
    nivelgalonesm3 = numerosGalonesm3[6];
  }
  if ((receptorlvm3 > prom8) && (receptorlvm3 <= prom9)) {
    nivelgalonesm2 = numerosGalonesm2[7];
  }
  if ((receptorlvm3 > prom9) && (receptorlvm3 <= prom10)) {
    nivelgalonesm3 = numerosGalonesm3[8];
  }
  if ((receptorlvm3 > prom10)) {
    nivelgalonesm3 = numerosGalonesm3[9];
  }






}
void tratamientonivel() {

  if (flagema1 == false) {
    emam1 = (nivelgalonesm1 * kema) + ((1 - kema) * nivelgalonesm1);
    flagema1 = true;
  } else {
    prb1 = ((1 - kema) * emam1);
    emam1 = (nivelgalonesm1 * kema) + prb1 ;
  }

}

void tratamientonivel2() {
  if (flagema2 == false) {
    emam2 = (nivelgalonesm2 * kema) + ((1 - kema) * nivelgalonesm2);
    flagema2 = true;
  } else {
    prb2 = ((1 - kema) * emam2);
    emam2 = (nivelgalonesm2 * kema) + prb2 ;
  }
}

void tratamientonivel3() {

  if (flagema3 == false) {
    emam3 = (nivelgalonesm3 * kema) + ((1 - kema) * nivelgalonesm3);
    flagema3 = true;
  } else {
    prb3 = ((1 - kema) * emam3);
    emam3 = (nivelgalonesm3 * kema) + prb3 ;
  }


}
//Funcion Tablas
void tablas() {
  byte datoIn[10];   //Longitud de bytes por renglón leído del archivo

  String datosInString[20];
  int contadorSaltosLinea = 0;
  int contadorBytesLinea = 0;
  //int contadorIndiceString = 0;
  int contadorSaltosPrueba = 0;
  int contadorComas = 0;

  myFile = SD.open("tanque1.csv", FILE_READ);
  if (myFile) {
    Serial.println("tanque1.csv");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      // Serial.print("holi");
      //Lectura Instantanea del dato
      byte lectura = myFile.read();
      //      Serial.println(" ");
      //      Serial.println(" ");
      Serial.write(lectura);
      //Si se encuentra un final de linea(10)
      //Incrementa el indice
      if (lectura == 10) {
        contadorSaltosPrueba ++;
      }

      if ((lectura == 10) && (contadorSaltosLinea == 0)) {
        contadorSaltosLinea++;
        contadorBytesLinea = 0;
        Serial.println("Primera fila ignorada.");

      }
      //Else if que realiza el proceso de Concantenado, y guardado en
      //los arreglos que almacenan de manera individual los datos
      else if ((lectura != 10) && (contadorSaltosLinea >= 1)) {

        if (lectura == 44) {

          switch (contadorComas) {
            case 0:
              datosPorcentajem1[contadorSaltosLinea - 1] = String((char *)datoIn);
              numerosPorcentajem1[contadorSaltosLinea - 1] = datosPorcentajem1[contadorSaltosLinea - 1].toFloat();
              Serial.print("Case 0 Línea String concatenada de la columna ");
              Serial.println(contadorComas + 1);
              Serial.println(numerosPorcentajem1[contadorSaltosLinea - 1]);
              contadorComas++;
              break;

            default:
              break;
          }
          Serial.println("Salida del switch.");
          //Se limpia el arreglo que
          //almacena datos parciales
          for (int i = 0; i < 10; i++) {
            datoIn[i] = byte(0);
          }
          contadorBytesLinea = 0;

        } else {
          //Control de Indices
          Serial.print("Guardando valores de la línea ");
          Serial.print(contadorSaltosLinea + 1);
          Serial.print(" del archivo en columna ");
          Serial.println(contadorComas + 1);

          //Lectura del archivo
          //Se tiene un arreglo parcial
          //que almacena los datos de los caracteres
          //que contiene cada dato antes de una coma
          datoIn[contadorBytesLinea] = lectura;
          Serial.write(datoIn[contadorBytesLinea]);
          Serial.println(" ");
          contadorBytesLinea++;

        }


      }
      //El concatenado de la ultima columna
      //siempre se hace al final
      else if ((lectura == 10) && (contadorSaltosLinea >= 1)) {

        datosGalonesm1[contadorSaltosLinea - 1] = String((char *)datoIn);
        numerosGalonesm1[contadorSaltosLinea - 1] = datosGalonesm1[contadorSaltosLinea - 1].toFloat();
        Serial.print("Línea String concatenada de la columna ");
        Serial.println(contadorComas + 1);
        Serial.println(numerosGalonesm1[contadorSaltosLinea - 1]);

        for (int i = 0; i < 10; i++) {
          datoIn[i] = byte(0);
        }


        Serial.println("Fin de la fila.");
        contadorSaltosLinea++;
        contadorComas = 0;
        contadorBytesLinea = 0;
      }
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  Serial.print("Cantidad de saltos de línea: ");
  Serial.println(contadorSaltosPrueba);
  Serial.println("Fin lectura, archivo cerrado. Datos obtenidos: ");


  Serial.println("Datos columna 1:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosPorcentajem1[i]);

  }

  Serial.println("Datos columna 2:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosGalonesm1[i]);
  }
  contadorSaltosLinea = 0;
  contadorBytesLinea = 0;
  //contadorIndiceString = 0;
  contadorSaltosPrueba = 0;
  contadorComas = 0;
  for (int i = 0; i < 10; i++) {
    datoIn[i] = byte(0);
  }

  myFile = SD.open("tanque2.csv", FILE_READ);
  if (myFile) {
    Serial.println("tanque2.csv");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      // Serial.print("holi");
      //Lectura Instantanea del dato
      byte lectura = myFile.read();
      //      Serial.println(" ");
      //      Serial.println(" ");
      Serial.write(lectura);
      //Si se encuentra un final de linea(10)
      //Incrementa el indice
      if (lectura == 10) {
        contadorSaltosPrueba ++;
      }

      if ((lectura == 10) && (contadorSaltosLinea == 0)) {
        contadorSaltosLinea++;
        contadorBytesLinea = 0;
        Serial.println("Primera fila ignorada.");

      }
      //Else if que realiza el proceso de Concantenado, y guardado en
      //los arreglos que almacenan de manera individual los datos
      else if ((lectura != 10) && (contadorSaltosLinea >= 1)) {

        if (lectura == 44) {

          switch (contadorComas) {
            case 0:
              datosPorcentajem2[contadorSaltosLinea - 1] = String((char *)datoIn);
              numerosPorcentajem2[contadorSaltosLinea - 1] = datosPorcentajem2[contadorSaltosLinea - 1].toFloat();
              Serial.print("Case 0 Línea String concatenada de la columna ");
              Serial.println(contadorComas + 1);
              Serial.println(numerosPorcentajem2[contadorSaltosLinea - 1]);
              contadorComas++;
              break;

            default:
              break;
          }
          Serial.println("Salida del switch.");
          //Se limpia el arreglo que
          //almacena datos parciales
          for (int i = 0; i < 10; i++) {
            datoIn[i] = byte(0);
          }
          contadorBytesLinea = 0;

        } else {
          //Control de Indices
          Serial.print("Guardando valores de la línea ");
          Serial.print(contadorSaltosLinea + 1);
          Serial.print(" del archivo en columna ");
          Serial.println(contadorComas + 1);

          //Lectura del archivo
          //Se tiene un arreglo parcial
          //que almacena los datos de los caracteres
          //que contiene cada dato antes de una coma
          datoIn[contadorBytesLinea] = lectura;
          Serial.write(datoIn[contadorBytesLinea]);
          Serial.println(" ");
          contadorBytesLinea++;

        }


      }
      //El concatenado de la ultima columna
      //siempre se hace al final
      else if ((lectura == 10) && (contadorSaltosLinea >= 1)) {

        datosGalonesm2[contadorSaltosLinea - 1] = String((char *)datoIn);
        numerosGalonesm2[contadorSaltosLinea - 1] = datosGalonesm2[contadorSaltosLinea - 1].toFloat();
        Serial.print("Línea String concatenada de la columna ");
        Serial.println(contadorComas + 1);
        Serial.println(numerosGalonesm2[contadorSaltosLinea - 1]);

        for (int i = 0; i < 10; i++) {
          datoIn[i] = byte(0);
        }


        Serial.println("Fin de la fila.");
        contadorSaltosLinea++;
        contadorComas = 0;
        contadorBytesLinea = 0;
      }
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  Serial.print("Cantidad de saltos de línea: ");
  Serial.println(contadorSaltosPrueba);
  Serial.println("Fin lectura, archivo cerrado. Datos obtenidos: ");


  Serial.println("Datos columna 1:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosPorcentajem2[i]);

  }

  Serial.println("Datos columna 2:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosGalonesm2[i]);
  }

  contadorSaltosLinea = 0;
  contadorBytesLinea = 0;
  //contadorIndiceString = 0;
  contadorSaltosPrueba = 0;
  contadorComas = 0;
  for (int i = 0; i < 10; i++) {
    datoIn[i] = byte(0);
  }

  myFile = SD.open("tanque3.csv", FILE_READ);
  if (myFile) {
    Serial.println("tanque3.csv");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      // Serial.print("holi");
      //Lectura Instantanea del dato
      byte lectura = myFile.read();
      //      Serial.println(" ");
      //      Serial.println(" ");
      Serial.write(lectura);
      //Si se encuentra un final de linea(10)
      //Incrementa el indice
      if (lectura == 10) {
        contadorSaltosPrueba ++;
      }

      if ((lectura == 10) && (contadorSaltosLinea == 0)) {
        contadorSaltosLinea++;
        contadorBytesLinea = 0;
        Serial.println("Primera fila ignorada.");

      }
      //Else if que realiza el proceso de Concantenado, y guardado en
      //los arreglos que almacenan de manera individual los datos
      else if ((lectura != 10) && (contadorSaltosLinea >= 1)) {

        if (lectura == 44) {

          switch (contadorComas) {
            case 0:
              datosPorcentajem3[contadorSaltosLinea - 1] = String((char *)datoIn);
              numerosPorcentajem3[contadorSaltosLinea - 1] = datosPorcentajem3[contadorSaltosLinea - 1].toFloat();
              Serial.print("Case 0 Línea String concatenada de la columna ");
              Serial.println(contadorComas + 1);
              Serial.println(numerosPorcentajem3[contadorSaltosLinea - 1]);
              contadorComas++;
              break;

            default:
              break;
          }
          Serial.println("Salida del switch.");
          //Se limpia el arreglo que
          //almacena datos parciales
          for (int i = 0; i < 10; i++) {
            datoIn[i] = byte(0);
          }
          contadorBytesLinea = 0;

        } else {
          //Control de Indices
          Serial.print("Guardando valores de la línea ");
          Serial.print(contadorSaltosLinea + 1);
          Serial.print(" del archivo en columna ");
          Serial.println(contadorComas + 1);

          //Lectura del archivo
          //Se tiene un arreglo parcial
          //que almacena los datos de los caracteres
          //que contiene cada dato antes de una coma
          datoIn[contadorBytesLinea] = lectura;
          Serial.write(datoIn[contadorBytesLinea]);
          Serial.println(" ");
          contadorBytesLinea++;

        }


      }
      //El concatenado de la ultima columna
      //siempre se hace al final
      else if ((lectura == 10) && (contadorSaltosLinea >= 1)) {

        datosGalonesm3[contadorSaltosLinea - 1] = String((char *)datoIn);
        numerosGalonesm3[contadorSaltosLinea - 1] = datosGalonesm3[contadorSaltosLinea - 1].toFloat();
        Serial.print("Línea String concatenada de la columna ");
        Serial.println(contadorComas + 1);
        Serial.println(numerosGalonesm3[contadorSaltosLinea - 1]);

        for (int i = 0; i < 10; i++) {
          datoIn[i] = byte(0);
        }


        Serial.println("Fin de la fila.");
        contadorSaltosLinea++;
        contadorComas = 0;
        contadorBytesLinea = 0;
      }
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  Serial.print("Cantidad de saltos de línea: ");
  Serial.println(contadorSaltosPrueba);
  Serial.println("Fin lectura, archivo cerrado. Datos obtenidos: ");


  Serial.println("Datos columna 1:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosPorcentajem3[i]);

  }

  Serial.println("Datos columna 2:");
  for (int i = 0; i < 10; i++) {

    Serial.println(numerosGalonesm3[i]);

  }

}

//Funcion de escritura
//en la memoria sd
void EscrituraSd() {
  // Creacion o Apertura de un archivo existente
  dia=day(); mes=month(); yea=year()%1000;
  message = String(dia)+String(mes)+String(yea)+".csv";
  //message = dateGPS + ".csv";
  Serial.println("Este es el Nombre del archivo: " + message);

  //message = "241120.csv";
  char  nombre[message.length()];
  message.toCharArray(nombre, message.length() + 1);
  nombreFile = (char *)nombre;
  myFile = SD.open(nombreFile, FILE_WRITE);
  if (myFile) {
    myFile.print(flujomediom1, 4);
    myFile.print(",");
    myFile.print(consumotiempopromediom1, 4);
    myFile.print(",");
    myFile.print(flujomediom2, 4);
    myFile.print(",");
    myFile.print(consumotiempopromediom2, 4);
    //        myFile.print(",");
    //        myFile.print(flujomediom3, 4);
    //        myFile.print(",");
    //        myFile.print(consumotiempopromediom3, 4);
    myFile.print(",");
    myFile.print(emam1, 4);
    myFile.print(",");
    myFile.print(emam2, 4);
    //        myFile.print(",");
    //        myFile.print(emam3, 4);
    myFile.println();
    myFile.close();

    
    ////////////////////////////////////////////////////////////////////////////////////////
  }
  ///// Funcion del smart one
//  crcval = crc16(valor, sizeof(valor) / sizeof(valor[0]));


  bncompletadom1 = false;
  bncompletadom2 = false;
  Serial.println("oooooooooooooooooo");
  Serial.println("oooooooooooooooooo");
  Serial.println("oooooooooooooooooo");

}



//Función que revisa lo recibido por el puerto serial
void revisionSerial(void) {
  message = Serial5.readString();
  Serial.println("");
  if (message == 'a') {
    Serial.print("Esto es un Minuto");
    tiempopromedio = 60000;
  }
  else  if (message == 'b') {
    Serial.print("Esto son 2 Minutos");
    tiempopromedio = 120000;
  }

  else  if (message == 'c') {
    Serial.print("Esto son 3 Minutos");
    tiempopromedio = 180000;
  }

  else  if (message == 'd') {
    Serial.print("Esto son 4 Minutos");
    tiempopromedio = 240000;
  }

  else  if (message == 'e') {
    Serial.print("Esto son 5 Minutos");
    tiempopromedio = 300000;
  }
  else  if (message == 'f') {
    Serial.print("Esto son 10 Minutos");
    tiempopromedio = 600000;
  }
  else  if (message == 'g') {
    Serial.print("Esto son 15 Minutos");
    tiempopromedio = 900000;
  }
  else  if (message == 'h') {
    Serial.print("Esto son 20 Minutos");
    tiempopromedio = 1200000;
  }
  else  if (message == 'i') {
    Serial.print("Esto son 25 Minutos");
    tiempopromedio = 1500000;
  }
  else  if (message == 'j') {
    Serial.print("Esto son 30 Minutos");
    tiempopromedio = 1800000;
  }
  else  if (message == 'k') {
    Serial.println("Unidades galones");
    banderagalones=true;
    banderalitros=false;
    Serial.print("Bandera galones: ");
    Serial.println( banderagalones);
    Serial.print("Bandera litros: ");
    Serial.println(banderalitros);
  }
  else  if (message == 'l') {
    Serial.println("Unidades litros");
        banderagalones=false;
    banderalitros=true;
    Serial.print("Bandera galones: ");
    Serial.println( banderagalones);
    Serial.print("Bandera litros: ");
    Serial.println(banderalitros);
  }
  else  if (message == 'm') {
    Serial.println("Unidades Nudos");
        
        banderanudos=true;
    banderakm=false;
    Serial.print("Bandera nd: ");
    Serial.println( banderanudos);
    Serial.print("Bandera km: ");
    Serial.println(banderakm);
  }
  else  if (message == 'n') {
    Serial.println("Unidades Kilometros");
        banderanudos=false;
    banderakm=true;
    Serial.print("Bandera nd: ");
    Serial.println( banderanudos);
    Serial.print("Bandera km: ");
    Serial.println(banderakm);
  }
  else {
    EnvioDeArchivos();
  }
}

//Funcion que Gestiona el envio
//de archivos desde la tarjeta a
//la aplicacion python
void EnvioDeArchivos() {

  //Transicion de la variable String
  //Puntero Char para ejecucion de los metodos sd
  char  nombre[message.length()];
  message.toCharArray(nombre, message.length() + 1);
  nombreFile = (char *)nombre;
  Serial.print(message);

  // Creacion o Apertura de un archivo existente
  myFile = SD.open(nombreFile, FILE_WRITE);
  myFile.close();
  myFile = SD.open(nombreFile, FILE_READ);

  //Mientras el archivo este Disponible
  //Realiza envio de los bytes en su interior
  if (myFile) {
    Serial.println("Contenido de prueba.csv:");

    // Lee mientras hayan datos disponibles (lee byte a byte)
    while (myFile.available()) {
      //Lee el byte recibido desde la SD y lo almacena en la variable tipo byte
      datoLecturaSD = myFile.read();
      //Imprime en consola el dato leído interpretado según ASCII
      //Si se quiere ver el valor real que lee se debe usar print o println
      Serial.write(datoLecturaSD);
      Serial5.write(datoLecturaSD); //Envio Via Bluetooth
    }
    // Cierra el archivo
    myFile.close();
    Serial.println("Cierre del archivo");
    //Arreglo reservado para indicar a la aplicación
    //python que se termino el envio de un archivo
    //.csv
    byte arr[3] = {0x7A, 0xD, 0xA}    ;
    Serial5.write(arr, 3);
    Serial.println("Envie el carac");
  } else {
    // Si no puede abrir el archivo imprime error en monitor serial
    Serial.println("Error abiendo prueba.csv");
  }
}

void EngineDynamicParam(const tN2kMsg &N2kMsg) {

  unsigned char EngineInstance ;
  double EngineOilPress = 0;
  double EngineOilTemp = 0;
  double EngineCoolantTemp = 0;
  double AltenatorVoltage = 0;
  double  FuelRate = 0;
  double  EngineHours = 0;
  double EngineCoolantPress = 0;
  double EngineFuelPress = 0;
  int8_t  EngineLoad = 0;
  int8_t EngineTorque = 0;

  if (ParseN2kEngineDynamicParam(N2kMsg, EngineInstance, EngineOilPress, EngineOilTemp, EngineCoolantTemp, AltenatorVoltage,
                                 FuelRate, EngineHours, EngineCoolantPress, EngineFuelPress,
                                 EngineLoad, EngineTorque)    ) {
    Serial.print("Valor Instancia flj :  ");
    Serial.println(EngineInstance);
    contador++;
    if (EngineInstance == 0) {
      identificador[0] = 0x31;

      Serial.print("Valor id if inst 0 :  ");
      Serial.println(identificador[0]);
    }
    else if (EngineInstance == 1) {
      identificador[1] = 0x31;
      Serial.print("Valor id if inst 1 :  ");
      Serial.println(identificador[1]);
    }
    else if (EngineInstance == 2) {
      identificador[2] = 0x31;
      Serial.print("Valor id if inst 2 :  ");
      Serial.println(identificador[2]);
    }
    //Tener en cuenta en cuenta el contador
    if (contador == 2) {
      contador = 0;
      for (int i = 0; i < 3; i++) {
        if (identificador[i] == 0x30) {

          /////Alarma, Activar envio del mensaje
          Serial.print("Sensor ");
          Serial.print(i + 1);
          Serial.println(" desconectado");
        }
        if (identificador[i] == 0x31) {
          Serial.print("Sensor ");
          Serial.print(i + 1);
          Serial.println(" conectado");
        }

      }
      for (int i = 0; i < 3; i++) {
        identificador[i] = 0x30;
      }

    }

    Serial.print("Valor id :  ");
    Serial.write(identificador, 3);
    //if(contador)

    // Selecciona el motor 1
    if (EngineInstance == 0) {

      Serial.print("El flujo de combustible en el motor 1 es:  ");
      Serial.println(FuelRate);
      tiempoActualm1 = millis();
      medidaflujom1 = FuelRate;
      deltatm1 = tiempoActualm1 - tiempoAnteriorm1;
      tiempoenhorasm1 = deltatm1 / (3600000) ;
      consumoinstm1 = tiempoenhorasm1 * medidaflujom1;
      Acumuladorconsumom1 = Acumuladorconsumom1 + consumoinstm1;
      Serial.print("El Consumo acumulado en el motor 1 es:  ");
      Serial.println(Acumuladorconsumom1, 3);
      deltaacumulativom1 = deltaacumulativom1 + deltatm1;
      Serial.print("El tiempo transcurrido(sg) es: ");
      Serial.println(deltaacumulativom1 / 1000);

      if (deltaacumulativom1 >= tiempopromedio) {
        consumotiempopromediom1 = Acumuladorconsumom1;
        deltaacumulativohorasm1 = deltaacumulativom1 / (3600000);
        flujomediom1 = Acumuladorconsumom1 / deltaacumulativohorasm1;

        Serial.println("---------");
        Serial.print("El flujo medio m1 es: ");
        Serial.println(flujomediom1, 4);
        Serial.print("El Consumo es: ");
        Serial.println(consumotiempopromediom1, 4);
        Serial.println("---------");
        Acumuladorconsumom1  = 0;
        deltaacumulativom1 = 0;
        bncompletadom1 = true;
      }
      //

      tiempoAnteriorm1 = tiempoActualm1;
      Serial.println("");
    }

    // Selecciona el motor 2
    if (EngineInstance == 1) {
      Serial.print("El flujo de combustible en el M2 es:  ");
      Serial.println(FuelRate);
      tiempoActualm2 = millis();
      medidaflujom2 = FuelRate;
      deltatm2 = tiempoActualm2 - tiempoAnteriorm2;
      tiempoenhorasm2 = deltatm2 / (3600000) ;
      consumoinstm2 = tiempoenhorasm2 * medidaflujom2;
      Acumuladorconsumom2 = Acumuladorconsumom2 + consumoinstm2;
      Serial.print("El Consumo acumulado en el M 2 es:  ");
      Serial.println(Acumuladorconsumom2, 3);
      deltaacumulativom2 = deltaacumulativom2 + deltatm2;
      Serial.print("El tiempo transcurrido(sg) es: ");
      Serial.println(deltaacumulativom2 / 1000);

      if (deltaacumulativom2 >= tiempopromedio) {
        consumotiempopromediom2 = Acumuladorconsumom2;
        deltaacumulativohorasm2 = deltaacumulativom2 / (3600000);
        flujomediom2 = Acumuladorconsumom2 / deltaacumulativohorasm2;

        Serial.println("---------");
        Serial.print("El flujo medio M2 es: ");
        Serial.println(flujomediom2, 4);
        Serial.print("El Consumo es: ");
        Serial.println(consumotiempopromediom2, 4);
        Serial.println("---------");
        Acumuladorconsumom2  = 0;
        deltaacumulativom2 = 0;
        bncompletadom2 = true;
      }
      //

      tiempoAnteriorm2 = tiempoActualm2;
      Serial.println("");
    }

    // Selecciona el motor 3
    if (EngineInstance == 2) {

      Serial.print("El flujo de combustible en el M3 es:  ");
      Serial.println(FuelRate);
      tiempoActualm3 = millis();
      medidaflujom3 = FuelRate;
      deltatm3 = tiempoActualm3 - tiempoAnteriorm3;
      tiempoenhorasm3 = deltatm3 / (3600000) ;
      consumoinstm3 = tiempoenhorasm3 * medidaflujom3;
      Acumuladorconsumom3 = Acumuladorconsumom3 + consumoinstm3;
      Serial.print("El Consumo acumulado en el M3 es:  ");
      Serial.println(Acumuladorconsumom3, 3);
      deltaacumulativom3 = deltaacumulativom3 + deltatm3;
      Serial.print("El tiempo transcurrido(sg) es: ");
      Serial.println(deltaacumulativom3 / 1000);

      if (deltaacumulativom3 >= tiempopromedio) {
        consumotiempopromediom3 = Acumuladorconsumom3;
        deltaacumulativohorasm3 = deltaacumulativom3 / (3600000);
        flujomediom3 = Acumuladorconsumom3 / deltaacumulativohorasm3;

        Serial.println("---------");
        Serial.print("El flujo medio M3 es: ");
        Serial.println(flujomediom3, 4);
        Serial.print("El Consumo es: ");
        Serial.println(consumotiempopromediom3, 4);
        Serial.println("---------");
        Acumuladorconsumom3  = 0;
        deltaacumulativom3 = 0;
        bncompletadom3 = true;
      }
      //

      tiempoAnteriorm3 = tiempoActualm3;
      Serial.println("");


    }
  }
}

void FluidLevel(const tN2kMsg &N2kMsg) {
  unsigned char Instance;
  tN2kFluidType FluidType;
  double Level = 0;
  double Capacity = 0;

  if (ParseN2kFluidLevel(N2kMsg, Instance, FluidType, Level, Capacity) ) {

    switch (FluidType) {
      case N2kft_Fuel:
        Serial.print("Nivel de gasolina ");
        break;
      case N2kft_Water:
        Serial.print("Water level: ");
        break;
      case N2kft_GrayWater:
        Serial.print("Gray water level :");
        break;
      case N2kft_LiveWell:
        Serial.print("Live well level :");
        break;
      case N2kft_Oil:
        Serial.print("Oil level :");
        break;
      case N2kft_BlackWater:
        Serial.print("Black water level :");
        break;
    }

    // Selecciona el tanque 1
    if (Instance == 0) {
      Serial.println("--------");
      Serial.print("en el tanque 1: ");
      Serial.print(Level); Serial.println("%");
      receptorlvm1 = Level;
      clasificacionnivel();
      tratamientonivel();
      Serial.print("los galones son: "); Serial.println(nivelgalonesm1);
      Serial.print("valor ema Tq1: "); Serial.println(emam1);

      Serial.println("--------");


    }

    // Selecciona el tanque 2
    if (Instance == 1) {

      Serial.println("--------");
      Serial.print("en el tanque 2: ");
      Serial.print(Level); Serial.println("%");
      receptorlvm2 = Level;
      clasificacionnivelm2 ();
      tratamientonivel2();
      Serial.print("los galones son: "); Serial.println(nivelgalonesm2);
      Serial.print("valor ema Tq2: "); Serial.println(emam2);
      Serial.println("--------");




    }

    // Selecciona el tanque 3
    if (Instance == 2) {

      Serial.println("--------");
      Serial.print("en el tanque 3: ");
      Serial.print(Level); Serial.println("%");
      receptorlvm3 = Level;
      clasificacionnivelm3 ();
      tratamientonivel3();
      Serial.print("los galones son: "); Serial.println(nivelgalonesm3);
      Serial.print("valor ema Tq3: "); Serial.println(emam3);
      Serial.println("--------");




    }
  }
}

//NMEA 2000 message handler
void HandleNMEA2000Msg(const tN2kMsg &N2kMsg) {
  int iHandler;
  // Find handler
  for (iHandler = 0; NMEA2000Handlers[iHandler].PGN != 0 && !(N2kMsg.PGN == NMEA2000Handlers[iHandler].PGN); iHandler++);
  if (NMEA2000Handlers[iHandler].PGN != 0) {
    NMEA2000Handlers[iHandler].Handler(N2kMsg);
  }
}
